import {LayoutTypeEnum} from './layout-type.enum';

export class LayoutOptions {

  public DEFAULT_THEME: LayoutTypeEnum = LayoutTypeEnum.DefaultTheme;

  public DARK_THEME: LayoutTypeEnum = LayoutTypeEnum.DarkTheme;

  public PANELLED_THEME: LayoutTypeEnum = LayoutTypeEnum.PanelledTheme;
}
