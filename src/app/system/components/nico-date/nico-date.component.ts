import {Component, Input, OnInit} from "@angular/core";
import {moment} from "ngx-bootstrap/chronos/test/chain";

@Component({
    selector: 'nico-date',
    template: "<span class='date nico-date'>{{output}}</span>"
})

export class NicoDateComponent implements OnInit {
    /**
     * The output value
     */
    public output: string = "";
    /**
     * Input value
     */
    @Input() value: string;

    @Input() format: string;

    /**
     *
     */
    public ngOnInit () {
        if (!this.format) {
            this.format = 'MM/DD/YYYY';
        }
        this.output = moment(this.value).utc(true).local().format(this.format);
    }

}
