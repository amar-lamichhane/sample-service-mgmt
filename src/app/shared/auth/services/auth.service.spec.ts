// import { TestBed, inject, async } from '@angular/core/testing';
//
// import { AuthService } from './auth.service';
// import { SharedModule } from '../../shared.module';
// import { NicoHttp } from 'src/app/system/requests/NicoHttp';
// import { NicoSession } from 'src/app/system/services/NicoSession';
// import { ToastNotification } from 'src/app/system/services/ToastNotification';
// import { AjaxSpinner } from 'src/app/system/services/AjaxSpinner';
// import { NicoModalController } from 'src/app/system/components/modal/NicoModalController';
// import { ViewController } from 'src/app/system/controllers/ViewController';
// import { PageTitle } from 'src/app/system/services/PageTitle';
// import { SmartValidationMessenger } from 'src/app/system/services/SmartValidationMessenger';
// import { RouterTestingModule } from '@angular/router/testing';
// import { HttpTestingController } from '@angular/common/http/testing';
// import { TranslateService, TranslateModule } from '@ngx-translate/core';
// import { ServerResponse } from 'src/app/system/responses/ServerResponse';
//
// describe('AuthService', () => {
//     const dummyUser = { username: 'sabin.koju@ensue.us', password: 'password' };
//     let authService: AuthService;
//     let http: NicoHttp;
//
//     beforeEach(() => {
//         TestBed.configureTestingModule({
//             imports: [
//                 SharedModule,
//                 RouterTestingModule,
//                 TranslateModule.forRoot()
//             ],
//             providers: [
//                 AuthService,
//                 NicoHttp,
//                 NicoSession,
//                 ToastNotification,
//                 AjaxSpinner,
//                 TranslateService,
//                 NicoModalController,
//                 ViewController,
//                 PageTitle,
//                 SmartValidationMessenger,
//                 HttpTestingController
//             ]
//         });
//     });
//
//     beforeEach(() => {
//         authService = new AuthService(http);
//     });
//
//     it('should be create login service', () => {
//         expect(authService).toBeTruthy();
//     });
//
//     it('should allow valid  user to login', async(inject([AuthService, HttpTestingController],
//         (service: AuthService, backend: HttpTestingController) => {
//             service.login(dummyUser).subscribe((res: ServerResponse) => {
//                 service.getAuthUser().subscribe(data => {
//                     expect(data).toBeDefined();
//                     expect(data.email).toBe(dummyUser.username);
//                 });
//             });
//         }
//     )));
//
//     it('should disallow invalid user to login', async(inject([AuthService, HttpTestingController],
//         (service: AuthService, backend: HttpTestingController) => {
//             service.login({ username: 'abc@gmail.com', password: '12345678' }).subscribe((res: ServerResponse) => {
//                 expect(res).toBeUndefined();
//             },
//                 (res: ServerResponse) => {
//                     expect(res).toBeDefined();
//                     expect(res.status.statusCode).toBe(401);
//                 }
//             );
//         }
//     )), 8000);
// });
