import {NgModule} from '@angular/core';

import {BrandLogoUrlPipe} from './brandLogoUrl.pipe';
import {DateFormatPipe} from './date-format.pipe';
import {FirstLetterPipe} from './firstLetter.pipe';
import {SafePipe} from './safe.pipe';
import {StatusPipe} from "./status.pipe";
import {StatusToggleCommandPipe} from "./status-toggle-command.pipe";

/**
 * Created by Amar on 2/5/2017.
 */
@NgModule({
    imports: [],
    declarations: [FirstLetterPipe, BrandLogoUrlPipe, DateFormatPipe, SafePipe, StatusPipe, StatusToggleCommandPipe],
    exports: [FirstLetterPipe, BrandLogoUrlPipe, DateFormatPipe, SafePipe, StatusPipe, StatusToggleCommandPipe],
    providers: []
})
export class PipesModule {
}
