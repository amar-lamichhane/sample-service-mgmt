import {AbstractBaseResource} from '../../../../../system/datamodels/AbstractBaseResource';
import {BaseSiteCategoryInterface} from '../interfaces/BaseSiteCategoryInterface';

export class BaseSiteCategory extends AbstractBaseResource implements BaseSiteCategoryInterface {
  /**
   * The primary key
   */
  public id: number;
  /**
   * The primary key identifier
   * @type {string}
   */
  primaryKey: string = 'id';
  /**
   * The creatable Attributes of the institutions
   */
  creatableAttributes: Array<string> = ['id', 'title', 'icon', 'description', 'status', 'created_at', 'updated_at'];

  [key: string]: any;

}
