/**
 * Created by Amar on 11/2/2017.
 */
export interface ForgotPasswordInterface {
    email: string;
}