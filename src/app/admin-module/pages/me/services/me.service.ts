import { EventEmitter, Injectable } from '@angular/core';
import { User } from '../models/User';
import { Observable } from 'rxjs';
import { UploadInput } from 'ngx-uploader';
import { map } from 'rxjs/internal/operators';
import { environment } from "../../../../../environments/environment";
import { BaseService } from "../../../../system/services/BaseService";
import { NicoHttp } from "../../../../system/requests/NicoHttp";
import { ServerResponse } from "../../../../system/responses/ServerResponse";
import { CredentialInterface } from 'src/app/shared/auth/Interfaces/CredentialInterface';

@Injectable()
export class MeService extends BaseService {

    /**
     * Upload file
     */
    uploadInput: EventEmitter<UploadInput>;
    /**
     * The resource
     * @type {string}
     */
    protected resourceName: string = 'auth';
    /**
     * Base resource url
     * @type {string}
     */
    protected resourceBaseUrl: string = '';
    /**
     *
     * @type {User}
     */
    protected resource: User;


    protected urlConfig: any = {
        login: 'authenticate',
        authUser: 'me',
        authPass: 'me/password',
        logout: 'logout'
    };

    /**
     * Constructor
     * @param http
     */
    public constructor(protected http: NicoHttp) {
        super(http, environment);
        this.resource = new User();
        this.setResourceName(this.resourceName);
    }

    public getUser() {
        return this.http.getAuthUser();
    }

    /**
     * Update profile
     */
    public updateProfile(credential: CredentialInterface): Observable<any> {
        return this.http.put(`${this.resourceBaseUrl}/${this.urlConfig.authUser}`, credential).pipe(map((d: ServerResponse) => {
            const user = this.resource.create(d.body);
            this.http.setAuthUser(user);
            return user;
        }));
    }

    /**
     * Update password
     */
    public updatePassword(credential: CredentialInterface): Observable<any> {
        return this.http.put(`${this.resourceBaseUrl}/${this.urlConfig.authPass}`, credential);
    }

    /**
     * to logout the user
     */
    public logout() {
        this.http.get(`${this.resourceBaseUrl}/${this.urlConfig.logout}`);
        this.http.getSession().clearAuth();
    }

    /**
     * upload file
     */
    public getAvatarUploadInputConfig(): UploadInput {
        return {
            type: 'uploadAll',
            url: `${this.resourceBaseUrl}/${this.urlConfig.authUser}/avatar`,
            method: 'POST',
            fieldName: 'image',
            headers: {
                'Authorization': 'Bearer ' + this.http.getSession().getToken()
            },
        };

    }

}
