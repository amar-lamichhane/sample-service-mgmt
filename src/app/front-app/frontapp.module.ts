import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FrontAppRoutingModule} from './front-app-routing.module';
import {SharedModule} from '../shared/shared.module';
import {FrontAppComponent} from './frontapp.component';
import {IndexComponent} from "./pages/index/index.component";

@NgModule({
  imports: [
    CommonModule,
    FrontAppRoutingModule,
    SharedModule.forRoot(),
  ],
  declarations: [FrontAppComponent, IndexComponent],
  exports: [FrontAppComponent, IndexComponent],

})
export class FrontAppModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: FrontAppModule,
      providers: []
    };
  }
}
