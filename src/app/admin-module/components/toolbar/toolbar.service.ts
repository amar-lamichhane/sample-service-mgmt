import {Injectable} from "@angular/core";
import {NicoHttp} from "../../../system/requests/NicoHttp";
import {map} from "rxjs/operators";
import {ServerResponse} from "../../../system/responses/ServerResponse";
import {BaseReseller} from "../../../shared/base/contituents/site/models/BaseReseller";
import {EnvironmentInterface} from "../../../system/datamodels/EnvironmentInterface";
import {environment} from "../../../../environments/environment";

@Injectable()
export class ToolbarService {
    protected environment: EnvironmentInterface;
    /**
     * Constructor
     * @param http
     * @param environment
     */
    public constructor(protected http:NicoHttp) {
        this.environment = environment;
    }

    /**
     * Get list of domains
     */
    public getDomains () {
        return this.http.get(this.environment.api + '/domains').pipe(map((response: ServerResponse) => {
            const reseller = new BaseReseller();
            return reseller.createFromCollection(response.body.data)
        }));
    }

}