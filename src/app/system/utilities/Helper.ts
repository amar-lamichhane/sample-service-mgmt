/**
 * The helper utilities class
 */
import {PlatformLocation} from "@angular/common";

export class Helper {

  static readonly DATE_FMT = 'MM.dd.yyyy';

  /**
   * Get the value from a given array
   * @param arr
   * @param key
   */
  static arrayGet(arr: Array<any>, key: string | number): any {
    if (!arr) {
      throw Error('Invalid array/object');
    }
    if (arr[<any>key]) {
      return arr[<any>key];
    } else {
      return null;
    }
  }

  static replaceString(subject: string, replace?: string, replaceWith?: string) {
    if (!replace) {
      replace = "";
    }
    if (!replaceWith) {
      replaceWith = '';
    }
    const regex = new RegExp('\\b' + replace + '\\b');
    subject = subject.replace(regex, replaceWith);
    return subject;
  }

  /**
   * Append text to a string, if the string doesn't exists yet
   * @param text
   * @param string
   */
  static appendTextIfDoesntExist(text: string, string: string): string {
    text = Helper.replaceString(text, string);
    text = text.trim();
    text += ' ' + string;
    return text;
  }

    /**
     * Assumes an original images has thumbnail with std [imagename]_[suffix].[extension]
     * @param url
     * @param suffix
     */
  static getThumbnailUrl(url: string, suffix?: string) {
    if (!suffix) {
      suffix = '_thumb';
    }
    let str: any = url.split('.');
    const ext = str.pop();
    str = str.join('.');
    str += suffix + '.' + ext;
    return str;
  }

  static getRandomString(chars?: number){
    if(isNaN(chars) || !chars) {
      chars = 10;
    }
    const charSets = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    let ret = "";
    for(let i=0;i<chars;i++) {
      ret +=charSets.charAt((Math.random() * 100 ) % charSets.length);
    }

    return ret;

  }

    /**
     * Get subdomain info from the platform location
     * @param location
     * @param defaultValue
     */
  static getSubdomain (location: PlatformLocation, defaultValue?: string) {
    const host = (<any>location).location.hostname;
    const splits = host.split('.');
    let domain: any = null;
    if(splits.length > 2) {
        domain = splits.shift();
    }
    if(!domain) {
      domain = defaultValue;
    }

    return domain;

  }


}
