import {Component, Input} from "@angular/core";
import {StatusEnum} from "../../enums/status.enum";
import {StatusOptions} from "../../enums/StatusOptions";

@Component ({
    selector: 'nico-status',
    template: '<span class="badge badge-pill " [class.badge-success]="value===statusOptions.STATUS_PUBLISHED" [class.badge-warning]="value===statusOptions.STATUS_UNPUBLISHED" [class.badge-danger]="value===statusOptions.STATUS_SUSPENDED">{{(value | status)|translate}}</span>',
})

export class NicoStatusComponent {

    @Input() value: StatusEnum;

    public statusOptions = new StatusOptions;

}
