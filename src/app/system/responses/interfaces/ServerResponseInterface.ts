/**
 * Created by Amar on 1/14/2017.
 */

export interface ServerResponseInterface {
  body: ServerResponseBodyInterface;
  status: ServerResponseStatusInterface;
}

export interface ServerResponseBodyInterface {
  [key: string]: any;
  data?: any;
}

export interface ServerResponseStatusInterface {
  message: string;
  code: string|number;
  statusCode: number;
  messageShown?: boolean;
}
