import { Component, EventEmitter, Injector, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';

import { BaseComponent } from "../../../system/controllers/BaseComponent";
import {NicoSession} from "../../../system/services/NicoSession";
import {AuthenticableInterface} from "../../../system/datamodels/AuthenticableInterface";
import {User} from "../../../shared/auth/models/User";
import {ToolbarService} from "./toolbar.service";
import {BaseResellerInterface} from "../../../shared/base/contituents/site/interfaces/BaseResellerInterface";
import {Collection} from "../../../system/utilities/Collection";

/**
 * This class represents the toolbar component.
 */
@Component({
    //moduleId: module.id,
    selector: 'admin-toolbar',
    templateUrl: 'toolbar.component.html',
    styleUrls: ['toolbar.component.scss'],
    providers: [ToolbarService],
})
export class AdminToolbarComponent extends BaseComponent implements OnInit, OnDestroy {

    public user: User;

    protected baseUserChangeSubscription: Subscription;

    public domains: Collection<BaseResellerInterface> = new Collection();

    public selectedDomain: BaseResellerInterface;

    public defaultDomain: BaseResellerInterface;

    /**
     * The event emitter for dashboard icon click
     */
    @Output('dashboardIconClick') dashboardIconClick: EventEmitter<null>;

    /**
     * Constructor
     * @param injector
     * @param session
     * @param toolbarService
     */
    public constructor(injector: Injector, protected session: NicoSession, protected toolbarService: ToolbarService) {
        super(injector);
        this.dashboardIconClick = new EventEmitter();
    }

    ngOnInit() {
        this.user = <User>this.session.getAuthUser();

        this.baseUserChangeSubscription = this.session.authUserUpdate().subscribe((d: AuthenticableInterface) => {
          this.user = <User> d;
        });

        this.toolbarService.getDomains().subscribe((resellers: Collection<BaseResellerInterface>) => {
            this.domains = resellers;

            this.selectedDomain = this.session.getDomain();
            const checkDomain = this.domains.first((item: BaseResellerInterface) => {
                return item.id === this.selectedDomain.id
            });
            if(!checkDomain) {
                this.selectedDomain = this.session.getDefaultDomain();
            }
        });


        this.selectedDomain = this.defaultDomain = this.session.getDefaultDomain();

    }

    ngOnDestroy() {
        this.baseUserChangeSubscription.unsubscribe();
    }

    /**
     * On domain select callback
     * @param item
     */
    public onDomainSelect(item: BaseResellerInterface) {
        if(item.id === this.selectedDomain.id) {
            return;
        }
        this.selectedDomain = item;
        if(!this.selectedDomain) {
            this.selectedDomain = this.session.getDefaultDomain();
        }
        this.session.setDomain (this.selectedDomain);
        this.appService.fireDomainChange(this.selectedDomain);

    }



    /**
     *
     */
    public onIconBarClick() {
        this.dashboardIconClick.emit();
    }

    public onUserIconClick() {
        this.router.navigate(['manage/me/profile']);
    }

    public getUserImage(): string {
        return this.user.thumb_url;
    }
}

