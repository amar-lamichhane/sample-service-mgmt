import {Component, EventEmitter, Injector, OnInit, Output, TemplateRef} from '@angular/core';
import {BaseComponent} from '../../../system/controllers/BaseComponent';
import {AdminSideNavbarService} from './admin-side-navbar.service';
import {Collection} from '../../../system/utilities/Collection';
import {AdminSidebarMenuItemInterface} from './admin-sidebar-menu-item.interface';
import {BreadcrumbsService} from '../../../system/components/breadcrumbs/BreadcrumbsService';
import {isNullOrUndefined} from 'util';

@Component({
  selector: 'admin-side-navbar',
  templateUrl: './admin-side-navbar.component.html',
  styleUrls: ['./admin-side-navbar.component.scss'],
})
export class AdminSideNavbarComponent extends BaseComponent implements OnInit {
  /**
   * Menu items that are shown on the sidebars
   */
  public menuItems: Collection<AdminSidebarMenuItemInterface>;

  public content: TemplateRef<any>;

  public collapsed: boolean = false;

  @Output() sideNavCollapsedEvent: EventEmitter<boolean>;

  /**
   * Constructor
   * @param injector
   * @param adminSideNavbar
   * @param breadcrumbs
   */
  constructor(injector: Injector, protected adminSideNavbar: AdminSideNavbarService, protected breadcrumbs: BreadcrumbsService) {
    super(injector);
    this.menuItems = new Collection();
    this.sideNavCollapsedEvent = new EventEmitter();
  }

  /**
   * On init hook
   */
  ngOnInit() {
    super.ngOnInit();

    this.adminSideNavbar.getMenuItems().subscribe((items: Collection<AdminSidebarMenuItemInterface>) => {
      this.menuItems.setItems(items.all());
    });

    this.adminSideNavbar.getTemplate().subscribe((template: TemplateRef<any>) => {
      this.content = template;
      this.collapsed = false;
      this.sideNavCollapsedEvent.emit(this.collapsed);
    });

  }

  onBackArrowClick(event: MouseEvent) {
    event.stopPropagation();
    const breadrumb = this.breadcrumbs.popItem();
  }

  /**
   * emit collapsed state
   * @param $event
   */

  toggleSideNav($event) {
    $event.preventDefault();
    if (isNullOrUndefined(this.content)) {
      this.collapsed = !this.collapsed;
      this.sideNavCollapsedEvent.emit(this.collapsed);
    }
  }
}
