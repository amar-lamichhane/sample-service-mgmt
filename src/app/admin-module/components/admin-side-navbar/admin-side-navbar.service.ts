import { Observable } from "rxjs";
import {AdminSidebarMenuItemInterface} from "./admin-sidebar-menu-item.interface";
import {Collection} from "../../../system/utilities/Collection";
import {EventEmitter, TemplateRef} from "@angular/core";

export class AdminSideNavbarService {

    protected menuItems: Collection<AdminSidebarMenuItemInterface>;

    protected menutItemChanges: EventEmitter<Collection<AdminSidebarMenuItemInterface>>;

    protected templateObserver: EventEmitter<TemplateRef<any>>;

    protected currentTemplate;


    /**
     * Get default menu item
     */
    protected getDefaultMenuItems (): Collection<AdminSidebarMenuItemInterface> {
        return new Collection<AdminSidebarMenuItemInterface>([
            {
                routerLink: 'dashboard',
                title: 'mod_manage.mod_admin_side_navbar.dashboard_label',
                icon: 'fas fa-tachometer-alt',
                active: true,
            },
            {
                routerLink: 'sites',
                title: 'mod_manage.mod_admin_side_navbar.sites_label',
                icon: 'fas fa-globe',
                active: true,
            },
            {
                routerLink: 'resellers',
                title: 'mod_manage.mod_admin_side_navbar.resellers_label',
                icon: 'fas fa-users',
                active: true,
            },

            {
                routerLink: 'videos',
                title: 'mod_manage.mod_admin_side_navbar.videos_label',
                icon: 'fas fa-video',
                active: false,
            },
            {
                routerLink: 'calculators',
                title: 'mod_manage.mod_admin_side_navbar.calculators_label',
                icon: 'fas fa-calculator',
                active: false,
            },
            {
                routerLink: 'video-buckets',
                title: 'mod_manage.mod_admin_side_navbar.video_bucket_label',
                icon: 'fab fa-bitbucket',
                active: false,
            },
            {
                routerLink: 'users',
                title: 'mod_manage.mod_admin_side_navbar.users_label',
                icon: 'fa fa-user',
                active: false,
            },
        ]);
    }


    public constructor() {
        this.menuItems = this.getDefaultMenuItems();
        this.menutItemChanges = new EventEmitter();
        this.templateObserver = new EventEmitter();
    }

    /**
     * Get menu items
     */
    public getMenuItems(): Observable<Collection<AdminSidebarMenuItemInterface>> {
        setTimeout(() => {
            this.menutItemChanges.emit(this.menuItems);
        },10);

        return this.menutItemChanges;
    }

    /**
     * Set the menu items. If null is given, the menu item is reverted back to defaults
     * @param items
     */
    public setMenuItems(items?: Collection<AdminSidebarMenuItemInterface>) {
        if(!items) {
            items = this.getDefaultMenuItems();
        }
        this.menutItemChanges.emit(items);
    }

    /**
     * Get the template
     */
    public getTemplate () {
        return this.templateObserver;
    }
    /**
     * Set template
     * @param content
     */
    public setTemplate(content: TemplateRef<any>) {
        this.currentTemplate = content;
        this.templateObserver.emit(content);
    }

    public getCurrentTemplate() {
        return this.currentTemplate;
    }


}

