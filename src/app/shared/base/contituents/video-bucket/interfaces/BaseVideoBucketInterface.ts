import { BaseResourceInterface } from "../../../../../system/datamodels/BaseResourceInterface";

export interface BaseVideoBucketInterface extends BaseResourceInterface {
    [key: string]: any;
}
