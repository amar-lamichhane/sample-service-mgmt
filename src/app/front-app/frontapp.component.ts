import {Component, Injector, OnInit} from '@angular/core';
import {BaseComponent} from '../system/controllers/BaseComponent';
/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  //moduleId: module.id,
  selector: 'front-app',
  templateUrl: 'forntapp.component.html',
  styleUrls: ['frontapp.component.scss'],

})
export class FrontAppComponent extends BaseComponent implements OnInit {

  public domain: string;

  protected fetchingToken:boolean = false;

  constructor (injector: Injector) {
    super(injector);
  }


  ngOnInit() {
      super.ngOnInit();
  }

}
