
import { EditPasswordComponent } from './components/edit-password/edit-password.component';
import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { AuthService } from '../../../shared/auth/services/auth.service';
import { BaseUser } from '../../../shared/base/contituents/user/models/BaseUser';
import { UploadFileOptionsInterface } from '../../../shared/upload-file-component/upload-file.options.interface';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { MeService } from './services/me.service';
import {BaseComponent} from "../../../system/controllers/BaseComponent";
import {Spinner} from "../../../system/services/AjaxSpinner";
import {ServerResponse} from "../../../system/responses/ServerResponse";


@Component({
    //moduleId: module.id,
    selector: 'cmp-me',
    templateUrl: 'me.component.html',
    styleUrls: [],

})
export class MeComponent extends BaseComponent {


    constructor(injector: Injector, public service: MeService, protected authService: AuthService) {
        super(injector);

    }
}
