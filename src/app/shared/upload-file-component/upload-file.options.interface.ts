import { UploadInput } from 'ngx-uploader';
import { Observable } from "rxjs";
export interface UploadFileOptionsInterface {
    title: string;
    iconClassName?: string;
    hidden?: boolean;
    inputId?: string;
    instantUpload?: boolean;
    fileInputHidden?: boolean;
    labelInputHidden?: boolean;
    uploadSuccessMessage?: string;
    displayProgressBar?: boolean;
    progressBarAttachment?: 'attached' | 'detached';
    autoHide?: boolean;
    hideTimeout?: number;
    cancellable?: boolean;
    progressFeedbackMessage?: string;
    onBeforeUpload?: Observable<any>;
    uploadInputConfig: UploadInput;
    disabledOnComplete?: boolean;
    dismissFeedbackOnComplete?: boolean;
}
