import { Component, EventEmitter, Injector, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { AuthService } from '../../../../../shared/auth/services/auth.service';
import { User } from '../../models/User';
import { MeService } from '../../services/me.service';
import {BaseComponent} from "../../../../../system/controllers/BaseComponent";
import {NicoModalActionInterface} from "../../../../../system/components/modal/NicoModalActionInterface";
import {Spinner} from "../../../../../system/services/AjaxSpinner";
import {ServerResponse} from "../../../../../system/responses/ServerResponse";

@Component({
    //moduleId: module.id,
    selector: 'cmp-edit-password',
    templateUrl: 'edit-password.component.html',

})
export class EditPasswordComponent extends BaseComponent implements NicoModalActionInterface, OnInit {

    @Output('done') onDone: EventEmitter<null>;
    /**
     * @var passwordForm
     */
    protected passwordForm: FormGroup;
    protected modalDismisser: Function;
    protected okayCompleteListener: Function;
    protected cancelCompleteListener: Function;
    /**
     * Busy
     */
    protected busy: boolean;
    /**
     * Constructor
     * @param {Injector} injector
     * @param {MeService} service
     * @param {AuthService} authService
     */
    public constructor(injector: Injector, protected service: MeService, protected authService: AuthService) {
        super(injector);
        this.onDone = new EventEmitter();
    }

    /**
     * On init event
     */
    public ngOnInit() {
        this.initPasswordForm();
    }

    /**
     * Init profile form
     */
    protected initPasswordForm() {
        this.passwordForm = this.formBuilder.group({
            old_password: [],
            new_password: [],
            new_password_confirmation: []
        });
    }

    public closeModal() {
        if (this.busy === true) {
            return;
        }
        this.modalDismisser ();
    }

    public save () {
        if (this.busy) {
            return;
        }
        this.busy = true;
        const spinner: Spinner = this.ajaxSpinner.showSpinner();
        this.service.updatePassword(this.passwordForm.value)
            .subscribe((d: User) => {
                this.busy = false;
                spinner.hide();
                this.passwordForm.reset();
                this.showSuccessToast('mod_manage.mod_profile.password_edit_success_message');
                this.modalDismisser();
            }, (d: ServerResponse) => {
                this.busy = false;
                spinner.hide();

                if (d.status.statusCode === 417) {
                    this.validationMessenger.attach('password-update-form', d.body);
                }
            });
    }

    onNicoModalOkayClick() {

    }

    onNicoModalCancelClick() {
    }

    onNicoModalClose() {
    }

    canDismissNicoModalOnOkay(): boolean {
        return false;
    }

    canDismissNicoModalOnCancel(): boolean {
        return true;
    }

    setNicoModalDismisser(fn: Function) {
        this.modalDismisser = fn;
    }

    setOkayProcessCompleteListener(fn: Function) {
        this.okayCompleteListener = fn;
    }
    setCancelProcessCompleteListener(fn: Function) {
        this.cancelCompleteListener = fn;
    }
    setNicoModalChildData() {
    }

}
