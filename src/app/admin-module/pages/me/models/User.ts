import { UserPhoneNumber } from "../../../../shared/auth/models/UserPhoneNumber";
import { BaseUser } from "../../../../shared/base/contituents/user/models/BaseUser";

export class User extends BaseUser {

    id: number;
    first_name: string;
    middle_name: string;
    last_name: string;
    image_url: string;
    thumb_url: string;
    username: string;
    primary_phone: UserPhoneNumber;
    email: string;

}
