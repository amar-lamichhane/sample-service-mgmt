import {AppComponent} from './app.component';
import {ConfirmConfigInterface} from './shared/confirm-dialog/confirm-config.interface';
import {NicoModalComponent} from "./system/components/modal/NicoModal";
import {EventEmitter} from "@angular/core";
import {BaseResellerInterface} from "./shared/base/contituents/site/interfaces/BaseResellerInterface";

export class AppService {
    /**
     * The app component
     */
    protected appComponent: AppComponent;

    protected domainChanged: EventEmitter<BaseResellerInterface>;

    public constructor () {
        this.domainChanged = new EventEmitter();
    }

    /**
     * Set the app component
     * @param {AppComponent} app
     */
    public setAppComponent(app: AppComponent) {
        this.appComponent = app;
    }

    public showConfirmDialog(config: ConfirmConfigInterface): NicoModalComponent {
        return this.appComponent.showConfirmDialog(config);
    }

    /**
     * Get domain change event
     */
    public getDomainChangeEvent (): EventEmitter<BaseResellerInterface> {
        return this.domainChanged;
    }

    /**
     * FireDomainChange
     * @param domain
     */
    public fireDomainChange (domain: BaseResellerInterface) {
        this.domainChanged.emit(domain);
    }

}
