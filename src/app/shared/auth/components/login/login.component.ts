import { Component, OnInit, OnDestroy, Host, Injector } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { FormGroup, Validators } from '@angular/forms';
import { User } from '../../models/User';
import { BaseComponent } from "../../../../system/controllers/BaseComponent";
import { NicoModalComponent } from "../../../../system/components/modal/NicoModal";
import { Spinner } from "../../../../system/services/AjaxSpinner";
import { ServerResponse } from "../../../../system/responses/ServerResponse";

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
    //moduleId: module.id,
    selector: 'sd-auth',
    templateUrl: 'login.component.html',
    styleUrls: [],

})
export class LoginComponent extends BaseComponent implements OnInit, OnDestroy {
    /**
     * The submitForm form
     * @type FormGroup
     */
    public loginForm: FormGroup;
    /**
     * Is submitForm submit busy
     * @type {boolean}
     */
    public loginSubmitBusy: boolean = false;

    public comp: NicoModalComponent;

    public response: any;

    /**
     * The constructor
     * @param {AuthService} authService
     * @param {Injector} injector
     */
    public constructor(protected authService: AuthService,
        injector: Injector) {
        super(injector);
    }

    /**
     * Ng On init implemented method
     */
    public ngOnInit() {
        this.setPageTitle('mod_login.module_title');
        this.initLoginForm();
    }

    /**
     * On onDestroy life cycle message
     */
    public ngOnDestroy() {
    }

    /**
     * Init the submitForm form
     */
    private initLoginForm() {
        this.loginForm = this.formBuilder.group({
            username: ['', [Validators.compose([Validators.required])]],
            password: ['', Validators.required]
        });
    }

    /**
     * Login
     */
    public login(event: Event) {
        event.preventDefault();
        if (this.loginSubmitBusy) {
            return;
        }
        this.loginSubmitBusy = true;
        const spinner: Spinner = this.ajaxSpinner.showSpinner();
        this.authService.login(this.loginForm.value)
            .subscribe(
                (d: ServerResponse) => {
                    this.loginSubmitBusy = false;
                    spinner.hide();
                    // once logged in get authenticated user
                    this.getAuthUser();
                },
                (d: ServerResponse) => {
                    this.loginSubmitBusy = false;
                    spinner.hide();
                    if (d.status.statusCode === 417) {
                        this.validationMessenger.attach('loginForm', d.body);
                    }
                }
            );
    }

    /**
     * Get Auth user
     */
    public getAuthUser() {
        const spinner: Spinner = this.ajaxSpinner.showSpinner();
        this.authService.getAuthUser().subscribe((d: User) => {
            spinner.hide();
            this.router.navigate(['/manage/dashboard']);
        }, () => {
            spinner.hide();
        });
    }

}
