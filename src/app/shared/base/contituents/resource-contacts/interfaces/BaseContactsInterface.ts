import {BaseResourceInterface} from "../../../../../system/datamodels/BaseResourceInterface";

export interface BaseContactsInterface extends BaseResourceInterface {
  id: number;
  title?: string;
  value?: string;
  type?: string;
  description?: string;
  status?: number;
  order?: number;
  created_at?: string;
  category_id?: number;
  [key: string]: any;
}
