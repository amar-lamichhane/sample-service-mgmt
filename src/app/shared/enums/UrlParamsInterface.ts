export interface UrlParamsInterface {
    title?: string;
    date?: string;
    page?: number;
    sort_by?: any;
    sort_order?: 'asc' | 'desc';
    status?: number | boolean;
    [key: string]: any;
}
