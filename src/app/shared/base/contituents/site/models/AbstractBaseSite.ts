import {AbstractBaseResource} from '../../../../../system/datamodels/AbstractBaseResource';
import {BaseResellerInterface} from '../interfaces/BaseResellerInterface';
import {BaseUser} from '../../user/models/BaseUser';

export abstract class AbstractBaseSite extends AbstractBaseResource implements BaseResellerInterface {
  public id: number;
  /**
   * The primary key identifier
   * @type {string}
   */
  primaryKey: string = 'id';
  /**
   * The creatable Attributes of the institutions
   */
  creatableAttributes: Array<string> = ['sites_count', 'id', 'title', 'description', 'logo_url', 'status', 'message', 'domain', 'primary_color', 'secondary_color', 'created_at', 'updated_at','layout'];

  [key: string]: any;

  /**
   *
   * @param obj
   */
  create(obj: any): BaseResellerInterface {
    const ret = <any>super.create(obj);
    if (obj.admin) {
      ret.admin = (new BaseUser()).create(obj.admin);
    }
    return ret;
  }
}
