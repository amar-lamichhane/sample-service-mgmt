import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from '../../../shared/shared.module';
import { EditPasswordComponent } from './components/edit-password/edit-password.component';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { MeRoutingModule } from './me-routing.module';
import { MeComponent } from './me.component';
import { MeService } from './services/me.service';

@NgModule({
  imports: [CommonModule, MeRoutingModule, SharedModule.forRoot()],
  declarations: [MeComponent, EditProfileComponent, EditPasswordComponent],
  exports: [],
  entryComponents: [EditProfileComponent, EditPasswordComponent],
  providers: [MeService]
})
export class MeModule { }
