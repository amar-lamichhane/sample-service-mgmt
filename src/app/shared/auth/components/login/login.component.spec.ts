import { async, ComponentFixture, fakeAsync, TestBed, tick, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { LoginComponent } from './login.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { AuthService } from '../../services/auth.service';
import { NicoHttp } from 'src/app/system/requests/NicoHttp';
import { NicoSession } from 'src/app/system/services/NicoSession';
import { ToastNotification } from 'src/app/system/services/ToastNotification';
import { AjaxSpinner } from 'src/app/system/services/AjaxSpinner';
import { NicoModalController } from 'src/app/system/components/modal/NicoModalController';
import { ViewController } from 'src/app/system/controllers/ViewController';
import { PageTitle } from 'src/app/system/services/PageTitle';
import { SmartValidationMessenger } from 'src/app/system/services/SmartValidationMessenger';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { By } from '@angular/platform-browser';
import { ServerResponse } from 'src/app/system/responses/ServerResponse';



describe('LoginComponent', () => {
    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    let location: Location;
    let router: Router;
    const dummyUser = { username: 'sabin.koju@ensue.us', password: 'password' };

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LoginComponent],
            imports: [
                SharedModule,
                TranslateModule.forRoot(),
                RouterTestingModule,
                HttpClientTestingModule
            ],
            providers: [
                AuthService,
                NicoHttp,
                NicoSession,
                ToastNotification,
                TranslateService,
                AjaxSpinner,
                NicoModalController,
                ViewController,
                PageTitle,
                SmartValidationMessenger,
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        router = TestBed.get(Router);
        location = TestBed.get(Location);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should check form validity', () => {
        expect(component.loginForm.valid).toBeFalsy();
        component.loginForm.controls['username'].setValue('abc@gmaill.us');
        component.loginForm.controls['password'].setValue('newpassword');
        expect(component.loginForm.valid).toBeTruthy();
    });

    it('should not allow form to be submitted if there are empty fields', () => {
        const loginBtn = fixture.debugElement.query(By.css('button.btn-primary'));
        expect(loginBtn.nativeElement.disabled).toBe(true);
    });

    // login with invalid credential
    it('should disallow invalid credential user to login', async(inject([AuthService, HttpTestingController],
        (service: AuthService, backend: HttpTestingController) => {
            service.login({ username: 'abc@gmail.com', password: '12345678' }).subscribe((res: ServerResponse) => {
                expect(res).toBeUndefined();
            },
                (res: ServerResponse) => {
                    expect(res).toBeDefined();
                    expect(res.status.statusCode).toBe(401);
                }
            );
        }
    )), 8000);

    // login with valid credential
    it('should login user with valid credential to the system', async(inject([AuthService, HttpTestingController],
        (service: AuthService, backend: HttpTestingController) => {
            service.login(dummyUser).subscribe(res => {
                service.getAuthUser().subscribe(data => {
                    expect(data).toBeDefined();
                    expect(data.email).toBe(dummyUser.username);
                });
            });
        }
    )));


    it('should navigate to dashboard', () => {
        router.navigate(['/manage/dashboard']).then(() => {
            expect(location.path()).toBe('/manage/dashboard');
        });
    });

    // navigation - functionality after login
    // ================================================================================

    // it('should navigate to dashboard after successful login', async(() => {
    //     component.loginForm.controls['username'].setValue('sabin.koju@ensue.us');
    //     component.loginForm.controls['password'].setValue('password');
    //     const event = jasmine.createSpyObj('event', ['preventDefault']);

    //     // trigger login function
    //     component.login(event);
    //     fixture.detectChanges();
    //     console.log(location.path());
    //     expect(location.path()).toBe('/manage/dashboard');
    // }));
});
