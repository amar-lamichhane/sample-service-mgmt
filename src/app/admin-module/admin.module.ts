import {NgModule} from '@angular/core';
import {AdminComponent} from './admin.component';
import {AdminRouteModule} from './admin.route';
import {AdminToolbarComponent} from './components/toolbar/toolbar.component';
import {AdminCommonModule} from "./admin-common.module";

@NgModule({
    declarations: [
        AdminComponent, AdminToolbarComponent,
    ],
    imports: [
        AdminCommonModule.forRoot(),
        AdminRouteModule
    ],
    providers: [],
})
export class AdminModule {
}
