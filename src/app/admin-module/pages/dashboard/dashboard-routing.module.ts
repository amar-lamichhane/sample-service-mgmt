import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { DashboardComponent } from "./dashboard.component";
import { AnalyticsComponent } from "./components/analytics/analytics.component";
import { VideoAnalyticsComponent } from "./components/video-analytics/video-analytics.component";

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: DashboardComponent,
        children: [
          {
            path: '',
            component: AnalyticsComponent
          },
          {
            path: 'video-analytics',
            component: VideoAnalyticsComponent
          }
        ]
      },
      { path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
      },
    ])
  ],
  exports: [RouterModule]
})

export class DashboardRoutingModule {}
