import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AuthMiddleware } from '../shared/auth/middlewares/AuthMiddleware';
import { AdminComponent } from './admin.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'manage', component: AdminComponent, canActivate: [AuthMiddleware], children: [
                    {
                        path: '',
                        redirectTo: 'dashboard',
                        pathMatch: 'full'
                    },
                    {
                        path: 'dashboard',
                        loadChildren: './pages/dashboard/dashboard.module#DashboardModule'
                    },

                    {
                        path: 'me',
                        loadChildren: './pages/me/me.module#MeModule'
                    },
                ]
            }
        ])
    ],
    exports: [RouterModule]
})
export class AdminRouteModule {
}
