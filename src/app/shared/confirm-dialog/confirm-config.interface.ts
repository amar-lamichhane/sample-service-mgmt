import {Observable} from 'rxjs';

export interface ConfirmConfigInterface {
    title?: string | Observable<string>;
    message?: any;
    cancelLabel?: string | Observable<string>;
    confirmLabel?: string | Observable<string>;
    onConfirm?: Function;
    onCancel?: Function;
}
