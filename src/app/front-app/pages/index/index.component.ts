import {Component, Injector} from "@angular/core";
import {BaseComponent} from "../../../system/controllers/BaseComponent";

@Component ({
    selector: "front-app-index",
    templateUrl: "./index.component.html"
})

export  class IndexComponent extends BaseComponent {

    public constructor(injector: Injector) {
        super(injector);
    }


}