import {Pipe, PipeTransform} from '@angular/core';
import {DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl} from '@angular/platform-browser';

@Pipe({
    name: 'safe'
})
export class SafePipe implements PipeTransform {

    constructor(protected sanitizer: DomSanitizer) {
    }

    public transform(value: any, type: string): SafeHtml | SafeStyle | SafeScript | SafeUrl | SafeResourceUrl {
        let ret: any = '';
        switch (type) {

            case 'html':
                ret = this.sanitizer.bypassSecurityTrustHtml(value);
                break;
            case 'style':
                ret = this.sanitizer.bypassSecurityTrustStyle(value);
                break;
            case 'script':
                ret = this.sanitizer.bypassSecurityTrustScript(value);
                break;
            case 'url':
                ret = this.sanitizer.bypassSecurityTrustUrl(value);
                break;
            case 'resourceUrl':
                ret = this.sanitizer.bypassSecurityTrustResourceUrl(value);
                break;
            default:
                throw new Error(`Invalid safe type specified: ${type}`);
        }
        return ret;
    }
}