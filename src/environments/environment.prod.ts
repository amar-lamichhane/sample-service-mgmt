import {EnvironmentInterface} from "../app/system/datamodels/EnvironmentInterface";

export const environment: EnvironmentInterface = {
    production: false,
    login_page: '/auth/login',
    password_update_page: '/auth/password/update',
    api: 'http://learnyourcare.com/api',
    base: '',

    public_namespace: "public",
    manage_namespace: "manage",

    public_request_tries: 3,
    public_app_client_id: 3,
    public_app_client_secret: "V0h04Knbp11MjeBYT5ZR2EOmxhDOa5Dd81OSauuG",
    default_public_subdomain: "ensue"
};
