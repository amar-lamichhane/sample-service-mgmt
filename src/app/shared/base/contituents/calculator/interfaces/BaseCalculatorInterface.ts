import {BaseResourceInterface} from "../../../../../system/datamodels/BaseResourceInterface";

export interface BaseCalculatorInterface extends BaseResourceInterface {
  id: number;
  title?: string;
  url?: string;
  description?: string;
  status?: number;
  created_at?: string;
  [key: string]: any;
}
