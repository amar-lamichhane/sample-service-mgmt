export enum ContactTypeEnum {
  email = 1,
  phone  = 2,
  address = 4
}
