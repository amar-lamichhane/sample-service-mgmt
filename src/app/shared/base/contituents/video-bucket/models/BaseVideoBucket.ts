import { BaseVideoBucketInterface } from "../interfaces/BaseVideoBucketInterface";
import { AbstractBaseResource } from "src/app/system/datamodels/AbstractBaseResource";
import { Collection } from "src/app/system/utilities/Collection";

export class BaseVideoBucketModel extends AbstractBaseResource implements BaseVideoBucketInterface {
    public id: number;
    /**
     * The primary key identifier
     * @type {string}
     */
    primaryKey: string = "id";
    /**
     * The creatable Attributes of the institutions
     */
    creatableAttributes: Array<string> = ['id', 'title', 'description', 'status', 'created_at', 'videos_count'];

    [key: string]: any;

}
