

import { BaseUserInterface } from "../interfaces/UserBaseInterface";
import { UserPhoneNumber } from "../../../../auth/models/UserPhoneNumber";
import { AuthenticableInterface } from "../../../../../system/datamodels/AuthenticableInterface";
import { Collection } from "../../../../../system/utilities/Collection";
import {AbstractBaseResource} from "../../../../../system/datamodels/AbstractBaseResource";

export class BaseUser extends AbstractBaseResource implements BaseUserInterface, AuthenticableInterface {
  public id: number;
  /**
   * The primary key identifier
   * @type {string}
   */
  primaryKey: string = "id";
  /**
   * The creatable Attributes of the institutions
   */
  creatableAttributes: Array<string> = ['id', 'first_name', 'last_name', 'middle_name', 'image_url', 'thumb_url', 'email', 'username','status','created_at','updated_at'];
  [key: string]: any;
  /**
   * Create
   * @param obj
   */
  public create(obj: any): any {
    const ret: BaseUserInterface = <BaseUserInterface>super.create(obj);
    ret.thumb_url = ret.image_url;
    return ret;
  }

    /**
     * Get user's full name
     */
  public getFullName() {
    const user = <BaseUserInterface>this;
    //we're doing it to avoid typescript error
    let ret = user.first_name;
    if (user.middle_name) {
      ret += ' ' + user.middle_name;
    }
    ret += ' ' + user.last_name;
    return ret;
  }

}
