/**
 * The Base Resource Interface
 */

import {Collection} from '../utilities/Collection';
import {PaginatedCollection} from "../utilities/PaginatedCollection";
import {StatusEnum} from "../enums/status.enum";

export interface BaseResourceInterface {

    id:number;

    htmlId?: string;
    /**
     * The primary key identifier
     *  string
     */
    primaryKey: string;
    /**
     * The creatable Attributes
     *  Array<string>
     */
    creatableAttributes: Array<string>;
    /**
     * title of resource
     */
    title?: string;
    /**
     * Description of resource
     */
    description?: string;
    /**
     * Created at label
     */
    created_at?: string;
    /**
     * Updated at label
     */
    updated_at?: string;
    /**
     * Status
     */
    status?: StatusEnum;

    /**
     * Create
     * @param obj
     */
    create(obj: any): BaseResourceInterface;

    /**
     * Create a collection
     * @param items
     */
    createFromCollection(items: Collection<any>): Collection<BaseResourceInterface>;

    createFromPaginatedCollection(body: any ): PaginatedCollection<BaseResourceInterface>;


}
