import {Component, EventEmitter, Injector, Input, OnInit, Output} from '@angular/core';
import {BaseComponent} from '../../system/controllers/BaseComponent';
import {BaseCalculatorInterface} from '../base/contituents/calculator/interfaces/BaseCalculatorInterface';
import {BaseCalculator} from '../base/contituents/calculator/models/BaseCalculator';
import {isNullOrUndefined} from 'util';

@Component({
  selector: 'app-calculator-card',
  templateUrl: './calculator-card.component.html',
  styleUrls: ['./calculator-card.component.scss']
})
export class CalculatorCardComponent extends BaseComponent implements OnInit {

  @Input() options: CalculatorCardConfigInterface;
  @Input() calculator: BaseCalculatorInterface;

  @Output() onSelect: EventEmitter<any>;
  @Output() onDragDrop: EventEmitter<any>;

  constructor(injector: Injector) {
    super(injector);
    this.onSelect = new EventEmitter();
    this.onDragDrop = new EventEmitter();
  }

  ngOnInit() {
    if (isNullOrUndefined(this.options)) {
      this.options = {
        draggable: false,
        selectOptions: false,
        addAction: false
      };
    } else {
      this.setOptions();
    }
  }

  /**
   * set options
   */
  public setOptions() {
    this.options.draggable = isNullOrUndefined(this.options.draggable) ? false : this.options.draggable;
    this.options.selectOptions = isNullOrUndefined(this.options.selectOptions) ? false : this.options.selectOptions;
    this.options.addAction = isNullOrUndefined(this.options.addAction) ? false : this.options.addAction;
  }

  /**
   * emit select events
   * @param $event
   * @param modal
   */
  public onActionSelect($event, modal: BaseCalculator) {
    $event.stopPropagation();
    this.onSelect.emit({modal: modal});
  }

  /**
   * emit drop event
   * @param $event
   * @param modal
   */
  public onDrop($event, modal: BaseCalculator) {
    this.onDragDrop.emit({event: $event, modal: modal});
  }
}


export interface CalculatorCardConfigInterface {
  draggable?: boolean;
  selectOptions?: boolean;
  addAction?: boolean;
}
