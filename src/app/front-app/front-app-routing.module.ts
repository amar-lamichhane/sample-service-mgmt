import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {FrontAppComponent} from './frontapp.component';
import {PageNotFoundComponent} from '../shared/page-not-found/page-not-found.component';
import {IndexComponent} from "./pages/index/index.component";

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: FrontAppComponent,
        children: [
            {
              path: '',
              component: IndexComponent
            },
        ]
      },
      {
        path: '404',
        component: PageNotFoundComponent
      }
    ])
  ],
  exports: []
})
export class FrontAppRoutingModule {

}
