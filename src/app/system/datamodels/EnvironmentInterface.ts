export interface EnvironmentInterface {
    login_page: string;
    api: string;
    base: string;
    production: boolean;
    public_namespace: string;
    manage_namespace: string;
    password_update_page: string;
    public_request_tries: number;
    public_app_client_id: number;
    public_app_client_secret: string;
    default_public_subdomain?: string
}