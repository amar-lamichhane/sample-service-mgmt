import {CommonModule} from '@angular/common';
import {ModuleWithProviders, NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';

import {AdvCheckboxComponent} from './adv-checkbox/adv-checkbox.component';
import {AuthMiddleware} from './auth/middlewares/AuthMiddleware';
import {GuestMiddleware} from './auth/middlewares/GuestMiddleware';
import {ConfirmDialogComponent} from './confirm-dialog/confirm-dialog.component';
import {NavbarComponent} from './navbar/navbar.component';
import {NicoAcronymComponent} from '../system/components/nico-acronym/nico-acronym.component';
import {PaginatorComponent} from './pagination/paginator.component';
import {UploadFileComponent} from './upload-file-component/upload-file.component';
import {IconPickerModule} from 'ngx-icon-picker';
import {environment} from '../../environments/environment';
import {User} from './auth/models/User';
import {SystemModule} from '../system/system.module';
import {SideNavComponent} from './side-nav/side-nav.component';
import {NgxUploaderModule} from 'ngx-uploader';
import {TruncateTextPipe} from './pipes/truncate-text.pipe';
import {ActionFlyMenuComponent} from '../system/components/action-fly-menu/action-fly-menu.component';
import {OnScrollDirective} from './directives/on-scroll.directive';
import {ColorPickerModule} from 'ngx-color-picker';
import {BaseUserService} from './services/base-user.service';
import {UserSelectInputComponent} from './user-select-input/user-select-input.component';
import {NewUserComponent} from './new-user-component/new-user.component';
import {NgDragDropModule} from 'ng-drag-drop';
import {NgScrollbarModule} from 'ngx-scrollbar';
import {ContactTypePipe} from './pipes/contact-type.pipe';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { OnResizeDirective } from './directives/on-resize.directive';

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    NgxUploaderModule,
    ReactiveFormsModule,
    HttpModule,
    SystemModule.forRoot(environment, new User()),
    IconPickerModule,
    ColorPickerModule,
    NgScrollbarModule,
    NgDragDropModule.forRoot(),
  ],
  declarations: [
    PaginatorComponent,
    NavbarComponent,
    NicoAcronymComponent,
    UploadFileComponent,
    ConfirmDialogComponent,
    AdvCheckboxComponent,
    TruncateTextPipe,
    ContactTypePipe,
    OnScrollDirective,
    SideNavComponent,
    ActionFlyMenuComponent,
    NewUserComponent,
    UserSelectInputComponent,
    PageNotFoundComponent,
    OnResizeDirective
  ],
  providers: [
    BaseUserService
  ],
  entryComponents: [
    ConfirmDialogComponent,
    NewUserComponent
  ],
  exports: [
    PaginatorComponent,
    NavbarComponent,
    SideNavComponent,
    NicoAcronymComponent,
    UploadFileComponent,
    AdvCheckboxComponent,
    ConfirmDialogComponent,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IconPickerModule,
    RouterModule,
    SystemModule,
    TranslateModule,
    ColorPickerModule,
    NgxUploaderModule,
    TruncateTextPipe,
    ContactTypePipe,
    OnScrollDirective,
    ActionFlyMenuComponent,
    UserSelectInputComponent,
    NewUserComponent,
    NgDragDropModule,
    NgScrollbarModule,
    PageNotFoundComponent,
    OnResizeDirective
  ]
})
export class SharedModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        AuthMiddleware,
        GuestMiddleware
      ]
    };
  }
}
