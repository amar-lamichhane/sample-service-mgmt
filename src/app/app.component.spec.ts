import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { AuthModule } from './shared/auth/auth.module';
import { FrontAppModule } from './front-app/frontapp.module';
import { AdminModule } from './admin-module/admin.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from './app.module';
import { RouterTestingModule } from '@angular/router/testing';
import { AppService } from './app.service';
import { NicoHttp } from './system/requests/NicoHttp';
import { AjaxSpinner } from './system/services/AjaxSpinner';
import { ToastNotification } from './system/services/ToastNotification';
import { NicoSession } from './system/services/NicoSession';
import { PageTitle } from './system/services/PageTitle';
import { SmartValidationMessenger } from './system/services/SmartValidationMessenger';
import { ViewController } from './system/controllers/ViewController';
import { NicoModalController } from './system/components/modal/NicoModalController';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        AuthModule,
        FrontAppModule,
        AdminModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory
            , deps: [HttpClient]
          }
        }),
      ],
      declarations: [
        AppComponent
      ],
      providers: [NicoHttp,
        AjaxSpinner,
        ToastNotification,
        NicoSession,
        PageTitle,
        SmartValidationMessenger,
        ViewController,
        NicoModalController,
        AppService
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
