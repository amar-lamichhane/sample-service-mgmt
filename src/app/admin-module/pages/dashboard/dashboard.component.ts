import {Component, Injector, OnInit} from '@angular/core';
import {BaseComponent} from "../../../system/controllers/BaseComponent";
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends  BaseComponent implements OnInit {

  constructor(protected  injector:Injector) {
    super(injector);
  }

  ngOnInit() {
      this.setPageTitle('mod_manage.mod_dashboard.page_title');
      this.http.get(environment.api + '/dashboard').subscribe();
  }

}
