import {AbstractBaseSite} from './AbstractBaseSite';
import {BaseSiteCustomer} from './BaseSiteCustomer';
import {BaseSiteCategory} from '../../site-category/models/BaseSiteCategory';
import {Collection} from '../../../../../system/utilities/Collection';
import {isNullOrUndefined} from 'util';
import {LayoutTypeEnum} from '../../../../enums/layout-type.enum';

export class BaseSite extends AbstractBaseSite {
  /**
   * Customer
   */
  public customer: BaseSiteCustomer;

  public categories: Collection<BaseSiteCategory>;

  public layout: LayoutTypeEnum;

  /**
   * Create the model
   * @param obj
   */
  create(obj: any): BaseSite {
    const site = <BaseSite>super.create(obj);
    if (obj.customer) {
      site.customer = <BaseSiteCustomer>(new BaseSiteCustomer()).create(obj.customer);
    }

    if (isNullOrUndefined(site.layout)) {
      site.layout = LayoutTypeEnum.DefaultTheme;
    }

    if (obj.categories) {
      site.categories = <Collection<BaseSiteCategory>>(new BaseSiteCategory()).createFromCollection(obj.categories);
    }

    return site;
  }
}
