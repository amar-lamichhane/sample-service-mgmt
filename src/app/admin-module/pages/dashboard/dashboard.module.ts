import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { AnalyticsComponent } from './components/analytics/analytics.component';
import { VideoAnalyticsComponent } from './components/video-analytics/video-analytics.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    DashboardRoutingModule,
  ],
  declarations: [
    DashboardComponent,
    AnalyticsComponent,
    VideoAnalyticsComponent
  ]
})
export class DashboardModule { }
