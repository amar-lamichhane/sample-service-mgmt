import {BaseResourceInterface} from "../../../../../system/datamodels/BaseResourceInterface";

export interface BaseVideoInterface extends BaseResourceInterface {
  video_url?: string;
  video_thumbnail_url?: string;
  [key: string]: any;
}
