

export interface CredentialInterface {
  username: string;
  password: string;
}
