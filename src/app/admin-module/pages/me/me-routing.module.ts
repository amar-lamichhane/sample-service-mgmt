import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MeComponent } from './me.component';
import {EditProfileComponent} from "./components/edit-profile/edit-profile.component";

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '', component: MeComponent,
          children: [
              {
                  path: 'profile',
                  component: EditProfileComponent
              }
          ]

      }
    ])
  ],
  exports: [RouterModule],
})
export class MeRoutingModule { }
