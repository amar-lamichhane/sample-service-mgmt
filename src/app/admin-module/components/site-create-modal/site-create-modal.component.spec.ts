import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteCreateModalComponent } from './site-create-modal.component';

describe('EditVideoComponent', () => {
  let component: SiteCreateModalComponent;
  let fixture: ComponentFixture<SiteCreateModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteCreateModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteCreateModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
