import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {PlatformLocation} from "@angular/common";

import {isNullOrUndefined} from "util";
import {BaseResellerInterface} from "../../../shared/base/contituents/site/interfaces/BaseResellerInterface";
import {
    FlyMenuActionEnum,
    FlyMenuItemInterface
} from "../../../system/components/action-fly-menu/action-fly-menu.component";
import {StatusEnum} from "../../../system/enums/status.enum";

@Component({
    selector: "app-site-item",
    templateUrl: "./site-item.component.html"
})

export class SiteItemComponent implements OnInit{
    /**
     * Main item
     */
    @Input() value: BaseResellerInterface;
    /**
     * If the item has subsite count
     */
    @Input() showSubsiteCount: boolean;

    @Input() itemIdPrefix: string;

    /**
     * action output
     */
    @Output() action: EventEmitter<FlyMenuActionEnum>;
    /**
     * View more output
     */
    @Output() viewMore: EventEmitter<any>;
    /**
     * Fly menu add ons
     */
    @Input() flyMenuAddOns: FlyMenuItemInterface[] = [
    ];

    @Input() extraFilyMenuItems: FlyMenuItemInterface[] = [];

    @Input() descriptionProperty: string = "description";

    public loc: any;

    protected suspendFlyItem: FlyMenuItemInterface = {name: 'suspend', label: 'Suspend', active: false};

    protected activateFlyItem: FlyMenuItemInterface = {name: 'activate', label: 'Activate', active: false};

    /**
     * Constructor
     * @param location
     */
    public constructor (protected location: PlatformLocation) {
        this.action = new EventEmitter();
        this.viewMore = new EventEmitter();
    }

    /**
     * Lifecycle hook
     */
    public ngOnInit () {
        this.loc = (<any> this.location).location;
        if(isNullOrUndefined(this.value)) {
            this.value = <any> {};
        }
        if(isNullOrUndefined(this.itemIdPrefix)){
            this.itemIdPrefix = "";
        }

        this.initFlyMenu();
    }

    /**
     * Init the fly menu
     */
    protected initFlyMenu() {

        this.flyMenuAddOns = [this.suspendFlyItem, this.activateFlyItem];

        if(this.extraFilyMenuItems && this.extraFilyMenuItems) {
            this.flyMenuAddOns = this.flyMenuAddOns.concat(this.extraFilyMenuItems);
        }

        this.suspendFlyItem.active = false;
        this.activateFlyItem.active = false;

        if(this.value.status === StatusEnum.Suspended) {
            this.suspendFlyItem.active = false;
            this.activateFlyItem.active = true;
        } else if (this.value.status === StatusEnum.Published || this.value.status === StatusEnum.Unpublished) {
            this.suspendFlyItem.active = true;
            this.activateFlyItem.active = false;
        }


    }

    /**
     * On fly menu action event handler
     * @param event
     */
    public onFlyMenuAction(event: FlyMenuActionEnum) {
        this.action.emit(event);
    }

    /**
     * On view more click
     * @param $event
     */
    public onViewMoreLinkClick ($event: MouseEvent) {
        $event.preventDefault();
        this.viewMore.emit($event);
    }

}