import {ModuleWithProviders, NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {SiteItemComponent} from "./components/site-item/site-item.component";
import {SiteCreateModalComponent} from "./components/site-create-modal/site-create-modal.component";
import {AdminSideNavbarService} from "./components/admin-side-navbar/admin-side-navbar.service";
import {AdminSideNavbarComponent} from "./components/admin-side-navbar/admin-side-navbar.component";

@NgModule({
    declarations: [
        SiteItemComponent, SiteCreateModalComponent,AdminSideNavbarComponent
    ],
    imports: [
        SharedModule,
    ],
    entryComponents: [
        SiteCreateModalComponent,
    ],
    exports: [
        SharedModule, SiteItemComponent, SiteCreateModalComponent, AdminSideNavbarComponent
    ],
})
export class AdminCommonModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: AdminCommonModule,
            providers: [
                AdminSideNavbarService,
            ]
        };
    }
}
