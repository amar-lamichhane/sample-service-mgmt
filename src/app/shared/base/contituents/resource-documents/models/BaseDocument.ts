import {AbstractBaseResource} from '../../../../../system/datamodels/AbstractBaseResource';
import {BaseDocumentInterface} from '../interfaces/BaseDocumentInterface';

export class BaseDocument extends AbstractBaseResource implements BaseDocumentInterface {
  public id: number;

  /**
   * The primary key identifier
   * @type {string}
   */
  primaryKey: string = 'id';
  /**
   * The creatable Attributes of the institutions
   */
  creatableAttributes: Array<string> = ['id', 'title', 'description', 'url', 'status', 'category_id', 'order', 'created_at'];

  [key: string]: any;

}
