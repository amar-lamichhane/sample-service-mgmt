export interface NicoModalActionInterface {
  onNicoModalOkayClick: Function;
  onNicoModalCancelClick: Function;
  onNicoModalClose: Function;
  canDismissNicoModalOnOkay: Function;
  canDismissNicoModalOnCancel: Function;
  setNicoModalDismisser: Function;
  setNicoModalChildData: Function;
  setOkayProcessCompleteListener: Function,
  setCancelProcessCompleteListener: Function,
}
