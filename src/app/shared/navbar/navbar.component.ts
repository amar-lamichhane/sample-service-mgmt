import { Component } from '@angular/core';

/**
 * This class represents the navigation bar component.
 */
@Component({
  selector: 'app-shared-navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.scss'],
})
export class NavbarComponent { }
