import {
    Component,
    EventEmitter,
    Injector,
    OnInit,
    Output,
} from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';

import { AuthService } from '../../../../../shared/auth/services/auth.service';
import { User } from '../../models/User';
import { MeService } from '../../services/me.service';
import {BaseComponent} from "../../../../../system/controllers/BaseComponent";
import {Spinner} from "../../../../../system/services/AjaxSpinner";
import {ServerResponse} from "../../../../../system/responses/ServerResponse";
import {EditPasswordComponent} from "../edit-password/edit-password.component";
import {UploadFileOptionsInterface} from "../../../../../shared/upload-file-component/upload-file.options.interface";

@Component({
   // moduleId: module.id,
    selector: 'cmp-edit-profile',
    templateUrl: 'edit-profile.component.html',

})
export class EditProfileComponent extends BaseComponent implements OnInit {

    /**
     * @var userFormGroup
     */
    public userFormGroup: FormGroup;

    public formId = "userProfileForm";

    public avatarUploadOptions: UploadFileOptionsInterface;
    /**
     * Busy
     */
    protected busy: boolean;

    protected me: User;

    public thumbUrl = "";

    protected avatarSpinner: Spinner;

    /**
     * Constructor
     * @param {Injector} injector
     * @param {MeService} service
     * @param {AuthService} authService
     */
    public constructor(injector: Injector, protected service: MeService, protected authService: AuthService) {
        super(injector);
    }

    public avatarUploadStart ($event: any) {
        this.avatarSpinner = this.ajaxSpinner.showSpinner('avatar-container');
    }
    /**
     * Image upload response
     * @param $event
     */
    public avatarUploadSuccess($event: ServerResponse) {
        this.avatarSpinner.hide();
        if ($event.status.code === 'ok') {
            this.userFormGroup.controls['image_url'].setValue($event.body.path);
            this.thumbUrl = $event.body.path;
        } else {
            this.showErrorToast($event.body.file.pop());
        }
    }

    protected setAvatarUploadOptions() {
        this. avatarUploadOptions = {
            title: '',
            instantUpload: true,
            cancellable: false,
            disabledOnComplete: false,
            hidden: true,
            fileInputHidden: true,
            labelInputHidden: true,
            inputId: "avatar_update",
            displayProgressBar: false,
            dismissFeedbackOnComplete: true,

            uploadInputConfig: {
                type: 'uploadAll',
                url: this.service.getBaseApiUrl() + '/upload-resources',
                headers: { 'Authorization': 'Bearer ' + this.session.getToken() },
                method: 'POST',
                fieldName: 'file',
                data: { 'type': 'image' },

            }
        };
    }

    /**
     * On init event
     */
    public ngOnInit() {
        this.initProfileForm();
        this.getAuthUserDetail();
        this.setAvatarUploadOptions();
        this.setPageTitle('mod_manage.mod_profile.page_title');

    }

    public getAuthUserDetail() {
        const spinner: Spinner = this.ajaxSpinner.showSpinner();
        this.authService.getAuthUser().subscribe((d: User) => {
            this.me = d;
            this.thumbUrl = this.me.thumb_url;
            this.userFormGroup.patchValue(d);
            this.userFormGroup.updateValueAndValidity();
            spinner.hide();
        }, ( ) =>  {
            spinner.hide();
        });
    }

    /**
     * Init profile form
     */
    protected initProfileForm() {
        this.userFormGroup = this.formBuilder.group({
            username: [''],
            first_name: ['', [Validators.compose([Validators.required])]],
            last_name: ['', Validators.required],
            middle_name: [''],
            image_url: [''],
        });
    }

    /**
     * Open change password modal
     */
    public openChangePasswordModal() {
        this.nicoCtrl.create(EditPasswordComponent, {
            showModalHeader: false,
            showCrossButton: false,
            showModalFooter: false,
            modalClass: 'custom-modal',
            htmlTemplateAsIs: true

        } ).present();
    }

    /**
     * Define what happens when okay button on nico modal is clicked
     */
    save() {
        if (this.busy) {
            return;
        }
        this.busy = true;
        const spinner: Spinner = this.ajaxSpinner.showSpinner();
        this.service.updateProfile(this.userFormGroup.value)
            .subscribe((d: User) => {
                this.busy = false;
                spinner.hide();
                this.userFormGroup.patchValue(d);
                this.showSuccessToast('mod_manage.mod_profile.edit_success_message');

            }, (d: ServerResponse) => {
                this.busy = false;
                spinner.hide();
                if (d.status.statusCode === 417) {
                    this.validationMessenger.attach(this.formId, d.body);
                }

            });
    }

    back () {
        this.location.back();
    }

}
