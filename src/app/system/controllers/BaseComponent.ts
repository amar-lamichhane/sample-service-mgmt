import {ComponentFactoryResolver, Injector, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, ParamMap, Params, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

import {NicoModalController} from '../components/modal/NicoModalController';
import {NicoHttp} from '../requests/NicoHttp';
import {AjaxSpinner, AjaxSpinnerEnum} from '../services/AjaxSpinner';
import {NicoSession} from '../services/NicoSession';
import {PageTitle} from '../services/PageTitle';
import {SmartValidationMessenger} from '../services/SmartValidationMessenger';
import {ToastEnum, ToastNotification} from '../services/ToastNotification';

import {Location, PlatformLocation} from '@angular/common';
import {StatusOptions} from '../enums/StatusOptions';
import {StatusEnum} from '../enums/status.enum';
import {ServerResponse} from '../responses/ServerResponse';
import {BaseService} from '../services/BaseService';
import {AbstractBaseResource} from '../datamodels/AbstractBaseResource';
import {AppService} from '../../app.service';
import {PaginatedCollection} from '../utilities/PaginatedCollection';
import {UrlParamsInterface} from '../../shared/enums/UrlParamsInterface';
import {AdvanceSearchParamsInterface} from '../components/advanced-search/advanced-search.component';
import {Observable, Subject} from 'rxjs';
import {FlyMenuActionEnum} from "../components/action-fly-menu/action-fly-menu.component";
import {BaseResourceInterface} from "../datamodels/BaseResourceInterface";

export abstract class BaseComponent implements OnInit {

  /**
   * The toast notification object
   */
  protected toast: ToastNotification;
  /**
   * The form builder object
   */
  protected formBuilder: FormBuilder;
  /**
   * The Nico Http Object
   */
  protected http: NicoHttp;
  /**
   * The ajax Spinner object
   */
  protected ajaxSpinner: AjaxSpinner;
  /**
   * The page title service
   */
  protected pageTitle: PageTitle;
  /**
   * Old page title of the document
   */
  protected oldPageTitle: string;

  protected translate: TranslateService;

  protected validationMessenger: SmartValidationMessenger;

  protected router: Router;

  protected activatedRoute: ActivatedRoute;

  protected routeParams: ParamMap;

  protected session: NicoSession;

  protected queryParameters: Params;

  protected location: Location;

  protected platformLocation: PlatformLocation;

  public statusOptions: StatusOptions = new StatusOptions();
  /**
   * @type BaseService The main service for the component.
   */
  protected service: BaseService;
  /**
   * @type AppService The main app service
   */
  protected appService: AppService;
  /**
   * @type UrlParamsInterface Url params when fetching list of items from baseService
   */
  public urlParams: UrlParamsInterface = { page: 1, sort_by: 'title', sort_order: 'asc',};
  /**
   * The main items to be displayed  on list component
   */
  public items: PaginatedCollection<AbstractBaseResource> = new PaginatedCollection();

  /**
   * The Nico Modal Controller object
   */
  protected nicoCtrl: NicoModalController;


  /**
   * Constructor
   * @param injector
   */
  public constructor(injector: Injector) {
    this.toast = injector.get(ToastNotification);
    this.formBuilder = injector.get(FormBuilder);
    this.http = injector.get(NicoHttp);
    this.ajaxSpinner = injector.get(AjaxSpinner);
    this.nicoCtrl = injector.get(NicoModalController);
    this.pageTitle = injector.get(PageTitle);
    this.translate = injector.get(TranslateService);
    this.validationMessenger = injector.get(SmartValidationMessenger);
    this.router = injector.get(Router);
    this.activatedRoute = injector.get(ActivatedRoute);
    this.session = injector.get(NicoSession);
    this.location = injector.get(Location);
    this.nicoCtrl.getViewController().setComponentFactoryResolver(injector.get(ComponentFactoryResolver));
    this.platformLocation = injector.get(PlatformLocation);
    this.appService = injector.get(AppService);

  }

  /**
   *  the Method that is called after route param is initialized
   */
  public onRouteParamInitialized() {

  }

  /**
   * the init lifecycle method
   */
  public ngOnInit(): void {
    this.oldPageTitle = this.pageTitle.getTitle();
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.activatedRoute.queryParams.subscribe((queryParams: Params) => {
        this.queryParameters = queryParams;
        this.routeParams = params;
        this.onRouteParamInitialized();
      });

    });
  }

  /**
   * Set page title
   * @param langString
   * @param extras Extra string that will be appended to the main title
   */
  public setPageTitle(langString: string, extras?: string) {
    this.translate.get(langString).subscribe((d: string) => {
      let title = d;
      if (extras) {
        title += ": " + extras;
      }
      this.pageTitle.setTitle(title);
    });
  }
  /**
   *
   * @param string langString
   * @param ToastEnum type
   */
  public showToast(langString: string, type: ToastEnum) {
    this.translate.get(langString).subscribe((d: string) => {
      this.toast.showToast(d, null, type);
    });
  }

  /**
   * Show Success toast
   * @param string langString
   */
  public showSuccessToast(langString: string) {
    this.translate.get(langString).subscribe((d: string) => {
      this.toast.showToast(d, null, ToastEnum.Success);
    });
  }

  /**
   * Show Error Toast
   * @param string langString
   */
  public showErrorToast(langString: string) {
    this.translate.get(langString).subscribe((d: string) => {
      this.toast.showToast(d, null, ToastEnum.Danger);
    });
  }

  /**
   * Show ban Toast
   * @param string langString
   */
  public showBanToast(langString: string) {
    this.translate.get(langString).subscribe((d: string) => {
      this.toast.showToast(d, null, ToastEnum.Ban);
    });
  }

  /**
   * Show Info toast
   * @param string langString
   */
  public showInfoToast(langString: string) {
    this.translate.get(langString).subscribe((d: string) => {
      this.toast.showToast(d, null, ToastEnum.Info);
    });
  }

  /**
   * Show warning toast
   * @param string langString
   *
   */
  public ShowWarningToast(langString: string) {
    this.translate.get(langString).subscribe((d: string) => {
      this.toast.showToast(d, null, ToastEnum.Warning);
    });
  }

  /**
   * Show default toast
   * @param string langString
   *
   */
  public ShowDefaultToast(langString: string) {
    this.translate.get(langString).subscribe((d: string) => {
      this.toast.showToast(d, null, ToastEnum.Default);
    });
  }

  /**
   * Toggle item's status
   * @param item
   */
  public togglePublish(item: AbstractBaseResource): Observable<any> {
    const ret = new Subject();
    this.service.togglePublish(item[item.primaryKey], item.status).subscribe((response) => {
      item.status = item.status === StatusEnum.Published ? StatusEnum.Unpublished : (item.status === StatusEnum.Unpublished ?
        StatusEnum.Published : item.status);
      return ret.next(response);
    }, (d: ServerResponse) => {
      if (!d.status.messageShown) {
        this.showErrorToast(d.status.message);
      }
      return ret.error(d);
    });
    return ret;
  }

  /**
   * Get list of items
   */
  public getList(): Observable<any> {
    const spinner = this.ajaxSpinner.showSpinner();
    const ret = new Subject();
    this.urlParams.domain = this.session.getDomain().domain;
    this.service.get({ params: this.urlParams }).subscribe((collection: PaginatedCollection<AbstractBaseResource>) => {
      spinner.hide();
      this.items = collection;
      return ret.next(collection);
    }, (response: ServerResponse) => {
      spinner.hide();
      if (!response.status.messageShown) {
        this.showErrorToast(response.status.message);
      }
      return ret.error(response);
    });
    return ret;
  }

  /**
   * On page change callback
   * @param page
   */
  public onPageChange(page: number) {
    this.urlParams.page = page;
    this.getList();
  }


  /**
   * Check if the collection is empty
   */
  public emptyList(): boolean {
    if (this.items) {
      return this.items.length === 0;
    }
    return true;
  }


  /**
   * Get delete confirmation message
   */
  protected getDeleteConfirmationMessage(): string {
    return 'mod_commons.delete_confirmation_message';
  }

  protected getDeleteSuccessMessage(): string {
    return "mod_commons.delete_success_message";
  }

  /**
   * Destroy the item
   * @param item
   */
  destroy(item: AbstractBaseResource) {
    this.appService.showConfirmDialog({
      message: this.getDeleteConfirmationMessage(),
      onConfirm: () => {
        const spinner = this.ajaxSpinner.showSpinner();
        this.service.destroy(item).subscribe(() => {
          spinner.hide();
          this.urlParams.page = 1;
          this.getList();
          this.showSuccessToast(this.getDeleteSuccessMessage());
        }, (resp: ServerResponse) => {
          spinner.hide();
          if (!resp.status.messageShown) {
            this.showErrorToast(resp.status.message);
          }
        }, () => {
          spinner.hide();
        });
      }
    }).present();
  }

    /**
     * Edit the item
     * @param item
     */
  public editItem (item: BaseResourceInterface) {

  }
    /**
     *
     * @param evt
     * @param item
     */
    public onFlyMenuAction(evt: FlyMenuActionEnum | any, item: any) {
        switch (evt) {
            case FlyMenuActionEnum.Remove:
                this.destroy(item);
                break;
            case FlyMenuActionEnum.Edit:
                this.editItem(item);
                break;
            case FlyMenuActionEnum.Status:
                this.togglePublish(item);
                break;
            default:

        }
    }


    /**
     * On search change method
     * @param params
     */
  public onSearchChange (params: AdvanceSearchParamsInterface) {
      this.urlParams.title = params.title;
      this.urlParams.status = <any>params.status;
      this.urlParams.sort_by =  params.sort_by;
      this.urlParams.sort_order = params.sort_order;
      this.urlParams.page = 1;
      this.getList();
  }


}
