export enum LayoutTypeEnum {
  DefaultTheme = 'light',
  DarkTheme = 'dark',
  PanelledTheme = 'panelled'
}
