import {EventEmitter, Inject} from '@angular/core';
import {Observable} from 'rxjs';
import {AuthenticableInterface} from '../datamodels/AuthenticableInterface';
import {BaseReseller} from "../../shared/base/contituents/site/models/BaseReseller";
import {BaseResellerInterface} from "../../shared/base/contituents/site/interfaces/BaseResellerInterface";

export class NicoSession {

    protected namespace: string = "";

    /**
     * The token key
     *  string
     *
     */
    private _tokenKey = 'auth_token';
    /**
     * The auth user
     *  string
     *
     */
    private _userKey = 'auth_user';

    private _domainKey = "selected_domain";
    /**
     * The authenticated user
     */
    private _authUser: any;
    /**
     * Auth user updated event handlers
     */
    private authUserUpdated: EventEmitter<any>;

    public constructor(@Inject('AuthenticatableInterface') protected auth: AuthenticableInterface) {
        this.authUserUpdated = new EventEmitter();
    }

    public authUserUpdate(): Observable<any> {
        return this.authUserUpdated;
    }

    public getNamespace (): string {
        return this.namespace;
    }

    public setNamespace(str: string) {
        this.namespace = str;
        return this;
    }


    /**
     * @return string
     */
    protected getFullyQualifiedKey(key:string) {
        return `${this.namespace}.${key}`;
    }

    /**
     * Set the auth token in the local storage for later use
     * This token gets appended in every following request unless modified otherwise
     * @param value
     * @param namespace
     */
    public setToken(value: string, namespace?: string) {
        localStorage.setItem(this.getFullyQualifiedKey(this._tokenKey), value);
    }

    /**
     * Get the current auth token
     * @return any
     */
    public getToken(): string {
        return localStorage.getItem(this.getFullyQualifiedKey(this._tokenKey));
    }

    /**
     * Set authenticated user in the session
     * @param user
     */
    public setAuthUser(user: AuthenticableInterface) {
        this.authUserUpdated.emit(user);
        this._authUser = user;
        localStorage.setItem(this.getFullyQualifiedKey(this._userKey), JSON.stringify(user));
    }

    /**
     * Get the authenticated user
     * @return any
     */
    public getAuthUser(): AuthenticableInterface {
        if (this._authUser != null) {
            return this._authUser;
        }
        let obj = localStorage.getItem(this.getFullyQualifiedKey(this._userKey));
        if (obj) {
            obj = JSON.parse(obj);
        }
        if (!obj) {
            return null;
        }
        this.setAuthUser(this.auth.create(obj));
        return this._authUser;
    }

    /**
     * Remove auth user and token from storage
     */
    public clearAuth() {
        localStorage.removeItem(this.getFullyQualifiedKey(this._tokenKey));
        localStorage.removeItem(this.getFullyQualifiedKey(this._userKey));
        localStorage.removeItem(this.getFullyQualifiedKey(this._domainKey));
        this._authUser = null;
    }

    public getDefaultDomain() : BaseResellerInterface {
        return <any>{id: null, title: 'Default', domain: 'www'};
    }

    /**
     * Set global domain
     * @param domain
     */
    public setDomain(domain: BaseResellerInterface) {
        localStorage.setItem(this.getFullyQualifiedKey(this._domainKey), JSON.stringify(domain));
    }

    /**
     * Get domain
     */
    public getDomain(): BaseResellerInterface {
        let obj: any = localStorage.getItem(this.getFullyQualifiedKey(this._domainKey));
        if(obj) {
            obj = JSON.parse(obj);
        }

        if(!obj) {
            obj = this.getDefaultDomain();
        }
        return obj;

    }


}
