import {BaseResourceInterface} from "../../../../../system/datamodels/BaseResourceInterface";

export interface BaseUserInterface extends BaseResourceInterface {
  id: number;
  first_name?: string;
  last_name?: string;
  middle_name?: string;
  email?: string;
  [key: string]: any;
  image_url?: string;
  thumb_url?: string;

  getFullName();

}
