import { ViewTypeEnum } from "../enums/view-type.enum";

export interface ViewTypeOptions {
    list: ViewTypeEnum.list;
    block: ViewTypeEnum.block;
}
