import {AbstractBaseResource} from '../../../../../system/datamodels/AbstractBaseResource';
import {BaseContactsInterface} from '../interfaces/BaseContactsInterface';

export class BaseContact extends AbstractBaseResource implements BaseContactsInterface {
  public id: number;

  /**
   * The primary key identifier
   * @type {string}
   */
  primaryKey: string = 'id';
  /**
   * The creatable Attributes of the institutions
   */
  creatableAttributes: Array<string> = ['id', 'title', 'description', 'value', 'type', 'status', 'category_id', 'order', 'created_at'];

  [key: string]: any;

}
