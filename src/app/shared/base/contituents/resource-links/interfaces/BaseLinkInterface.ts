import {BaseResourceInterface} from '../../../../../system/datamodels/BaseResourceInterface';

export interface BaseLinkInterface extends BaseResourceInterface {
  url?: string;
  [key: string]: any;
}
