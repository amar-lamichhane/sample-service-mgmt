import { Collection } from "../../../../../system/utilities/Collection";
import {BaseVideoInterface} from "../interfaces/BaseVideoInterface";
import {AbstractBaseResource} from "../../../../../system/datamodels/AbstractBaseResource";

export class BaseVideo extends AbstractBaseResource implements BaseVideoInterface  {
  public id: number;
  /**
   * The primary key identifier
   * @type {string}
   */
  primaryKey: string = "id";
  /**
   * The creatable Attributes of the institutions
   */
  creatableAttributes: Array<string> = ['id', 'title', 'description', 'video_url', 'video_thumbnail_url', 'status', 'created_at'];
  [
      key: string]: any;
  /**
   * Create
   * @param obj
   */
  public create(obj: any): BaseVideoInterface {
    const ret: BaseVideoInterface = super.create(obj);
    ret.video_thumbnail_url_small = ret.video_thumbnail_url;
    return ret;
  }

}
