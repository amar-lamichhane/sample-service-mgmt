import {Component, Injector} from '@angular/core';
import {BaseComponent} from '../system/controllers/BaseComponent';
import {environment} from "../../environments/environment";

@Component({
  selector: 'admin-root',
  templateUrl: './admin.component.html',
  styleUrls: ['admin.component.scss'],
})
export class AdminComponent extends BaseComponent {

  /**
   * The boolean to set if the sidebar is expanded or not
   * @type {boolean}
   */
  public flexCollapsed: boolean = false;

  /**
   * The constructor
   * @param {Injector} injector
   */
  public constructor(injector: Injector) {
    super(injector);
  }

  public ngOnInit () {
    super.ngOnInit();
    this.http.setNamespace(environment.manage_namespace);
  }

  /**
   * Called when route params are initialized
   */
  public onRouteParamInitialized() {

  }

  /**
   * Event handler when an toolbar icon is clicked
   */
  public onDashboardIconClicked($event: boolean) {
    this.flexCollapsed = $event;
  }
}
