import {AbstractBaseResource} from '../../../../../system/datamodels/AbstractBaseResource';
import {BaseLinkInterface} from '../interfaces/BaseLinkInterface';

export class BaseLink extends AbstractBaseResource implements BaseLinkInterface {
  public id: number;

  /**
   * The primary key identifier
   * @type {string}
   */
  primaryKey: string = 'id';
  /**
   * The creatable Attributes of the institutions
   */
  creatableAttributes: Array<string> = ['id', 'title', 'url', 'status', 'category_id', 'order', 'created_at'];

  [key: string]: any;
}
