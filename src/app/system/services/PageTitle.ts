

import {Title} from "@angular/platform-browser";
import {Injectable} from "@angular/core";

@Injectable()
export class PageTitle {

  private titleAddOn:string = "LearnYour Benefits";

  private titlePrefixed:boolean = true;

  private separator:string = "::";

  private addOnShown:boolean = true;

  /**
   *
   * @param title
   */
  public constructor(protected title:Title){

  }


  private createTitle(str:string):string{
    if(this.titleAddOn && this.addOnShown===true){
      if(this.titlePrefixed===true){
        str = `${this.titleAddOn} ${this.separator} ${str}`;
      }else{
        str = ` ${str} ${this.separator} ${this.titleAddOn} `;
      }
    }
    return str;
  }

  public showAddOn(show: boolean) {
    this.addOnShown = show;
  }

  public setPrefix(show: boolean) {
    this.titlePrefixed  = show;
  }

  /**
   * Set the title of the document
   * @param str
   */
  public setTitle (str:string):void{
    this.title.setTitle(this.createTitle(str));
  }

    /**
     * Get current page title
     */
  public getTitle (): string {
    return this.title.getTitle();
  }


}
