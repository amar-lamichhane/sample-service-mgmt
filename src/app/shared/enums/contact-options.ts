import {ContactTypeEnum} from './contact-type.enum';

export class ContactOptions {

  public CONTACT_TYPE_EMAIL: ContactTypeEnum = ContactTypeEnum.email;

  public CONTACT_TYPE_PHONE: ContactTypeEnum = ContactTypeEnum.phone;

  public CONTACT_TYPE_ADDRESS: ContactTypeEnum = ContactTypeEnum.address;

  private _items: Array<ContactTypeEnum> = [ContactTypeEnum.email, ContactTypeEnum.phone, ContactTypeEnum.address];

  get length() {
    return this._items.length;
  }

  [Symbol.iterator]() {
    return this._items.values();
  }
}
