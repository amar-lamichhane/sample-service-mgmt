import {Observable} from 'rxjs';
import {BaseVideoInterface} from '../base/contituents/video/interfaces/BaseVideoInterface';
import {Collection} from '../../system/utilities/Collection';
import {BaseDocumentInterface} from '../base/contituents/resource-documents/interfaces/BaseDocumentInterface';
import {BaseLinkInterface} from '../base/contituents/resource-links/interfaces/BaseLinkInterface';
import {BaseContactsInterface} from '../base/contituents/resource-contacts/interfaces/BaseContactsInterface';
import {BaseCalculatorInterface} from '../base/contituents/calculator/interfaces/BaseCalculatorInterface';

export interface CategoryResourceServiceInterface {

  setBaseUrl(url: string): void;

  getVideos(categoryId: number): Observable<Collection<BaseVideoInterface>>;

  getDocuments(categoryId: number): Observable<Collection<BaseDocumentInterface>>;

  getLinks(categoryId: number): Observable<Collection<BaseLinkInterface>>;

  getContacts(categoryId: number): Observable<Collection<BaseContactsInterface>>;

  getCalculators(categoryId: number): Observable<Collection<BaseCalculatorInterface>>;
}
