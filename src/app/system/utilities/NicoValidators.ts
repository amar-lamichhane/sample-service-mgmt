/**
 * Created by Amar on 2/5/2017.
 */
import { AbstractControl } from "@angular/forms";
/**
 * Extra validators definitions
 */
export class NicoValidators {
  /**
   * The url Validator
   * @param control
   * @return any
   */
  static url(control: AbstractControl): any {
    const test = /^http(s)?:\/\/(www.)?[a-zA-Z\w]+[a-zA-Z0-9\w\d]*\.([\w\d]+\.)*[a-zA-Z]+$/;
    if (test.test(control.value) === true) {
      return null;
    }
    return { url: true };
  }

}
