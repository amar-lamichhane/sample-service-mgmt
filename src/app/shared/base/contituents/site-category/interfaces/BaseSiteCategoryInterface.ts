import {BaseResourceInterface} from "../../../../../system/datamodels/BaseResourceInterface";

export interface BaseSiteCategoryInterface extends BaseResourceInterface {

  [key: string]: any;

}
