import { Collection } from "../../../../../system/utilities/Collection";
import {BaseCalculatorInterface} from "../interfaces/BaseCalculatorInterface";
import {AbstractBaseResource} from "../../../../../system/datamodels/AbstractBaseResource";

export class BaseCalculator extends AbstractBaseResource implements BaseCalculatorInterface  {
  public id: number;
  /**
   * The primary key identifier
   * @type {string}
   */
  primaryKey: string = "id";
  /**
   * The creatable Attributes of the institutions
   */
  creatableAttributes: Array<string> = ['id', 'title', 'description', 'url', 'status', 'created_at'];

  [key: string]: any;



}
