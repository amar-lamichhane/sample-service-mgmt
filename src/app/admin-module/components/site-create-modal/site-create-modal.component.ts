import {Component, Injector, OnInit} from '@angular/core';
import {NicoModalActionInterface} from '../../../system/components/modal/NicoModalActionInterface';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UploadFileOptionsInterface} from '../../../shared/upload-file-component/upload-file.options.interface';
import {BaseComponent} from '../../../system/controllers/BaseComponent';
import {StatusEnum} from '../../../system/enums/status.enum';
import {ServerResponse} from '../../../system/responses/ServerResponse';
import {Observable, Subject} from 'rxjs';
import {NicoModalActionEnum} from '../../../system/components/modal/NicoModal';
import {BaseResellerInterface} from '../../../shared/base/contituents/site/interfaces/BaseResellerInterface';
import {V} from '@angular/core/src/render3';

@Component({
  selector: 'app-create-site-modal',
  templateUrl: './site-create-modal.component.html',
  styleUrls: ['./site-create-modal.component.scss']
})
export class SiteCreateModalComponent extends BaseComponent implements OnInit, NicoModalActionInterface {
  /**
   * Modal dismisser
   */
  protected modalDismisser: Function;

  public modalTitle: string;

  protected service: any;
  /**
   * Resource
   */
  public resource: BaseResellerInterface = null;

  public formGroup: FormGroup;

  public thumbUploadOptions: UploadFileOptionsInterface = null;

  public module: string;

  public descriptionControlName = 'description';

  /**
   * Form id
   */
  public formId = 'edit-form';

  /**
   * Constructor
   * @param injector
   */
  constructor(injector: Injector) {
    super(injector);
  }

  protected setUploadOption() {
    this.setThumbUploadOption();
  }

  protected setThumbUploadOption() {
    this.thumbUploadOptions = {
      title: 'mod_manage.' + this.module + '.thumbnail_label',
      instantUpload: true,
      cancellable: false,
      disabledOnComplete: true,
      inputId: 'logo_url',
      hidden: true,
      uploadInputConfig: {
        type: 'uploadAll',
        url: this.service.getBaseApiUrl() + '/upload-resources',
        headers: {'Authorization': 'Bearer ' + this.session.getToken()},
        method: 'POST',
        fieldName: 'file',
        data: {'type': 'image'},
      }
    };
  }

  public ngOnInit() {
    super.ngOnInit();

    this.setUploadOption();
    if (this.module === 'mod_sites') {
      this.descriptionControlName = 'message';
    }
    this.formGroup = this.formBuilder.group({
      title: ['', Validators.compose([Validators.required])],
      domain: ['', Validators.compose([Validators.required])],
      logo_url: ['', Validators.compose([Validators.required])],
      primary_color: ['#000000'],
      secondary_color: ['#FFFFFF'],
      status: [StatusEnum.Unpublished, Validators.compose([Validators.required])],
    });
    const descriptionFormControl = new FormControl('', Validators.required);

    if (this.module === 'mod_sites') {
      this.formGroup.setControl('message', descriptionFormControl);
    } else {
      this.formGroup.setControl('description', descriptionFormControl);
    }

    this.modalTitle = 'mod_manage.' + this.module + '.add.page_title';
    this.setPageTitle('mod_manage.' + this.module + '.add.page_title');

    if (this.resource.id) {
      this.translate.get('mod_manage.' + this.module + '.edit.page_title').subscribe((val: string) => {
        val += ': ' + this.resource.title;
        this.modalTitle = val;
        this.setPageTitle(val);
      });
      this.formGroup.patchValue(this.resource);

    }

  }

  /**
   * Image upload response
   * @param $event
   */
  public imageUploadResponse($event: ServerResponse) {
    if ($event.status.code === 'ok') {
      this.formGroup.controls['logo_url'].setValue($event.body.path);
    } else {
      this.validationMessenger.attach(this.formId, $event.body);
      // this.showErrorToast($event.status.message);
    }
  }

  /**
   * Save the form data
   */
  public saveForm(): Observable<any> {
    const spinner = this.ajaxSpinner.showSpinner();
    const observable = new Subject();
    const data = this.formGroup.value;

    if (this.resource.id) {
      delete data.status;
    }
    this.service.save(data, this.resource ? this.resource.id : null).subscribe(() => {
      spinner.hide();
      return observable.next();
    }, (resp: ServerResponse) => {
      spinner.hide();
      if (!resp.status.messageShown) {
        this.showErrorToast(resp.status.message);
      }
      if (resp.status.statusCode === 417) {
        this.validationMessenger.attach(this.formId, resp.body);
      }
      throw resp;
    });
    return observable;
  }

  /**
   * Color change event handler
   * @param color
   * @param formControlName
   */
  public colorPickerChange(color: string, formControlName: string) {
    this.formGroup.controls[formControlName].setValue(color);
  }

  /**
   * On route param initialized hook
   */
  onRouteParamInitialized() {

  }

  /**
   * On form submit
   */
  public save() {
    this.saveForm().subscribe(res => {
      this.showSuccessToast('mod_manage.' + this.module + '.edit.success_message');
      this.modalDismisser(NicoModalActionEnum.Positive);
    });
  }

  /**
   * On cancel
   */

  public cancel() {
    this.modalDismisser(NicoModalActionEnum.Negative);
    return false;
  }

  /**
   * @override
   */
  public canDismissNicoModalOnCancel() {
    return false;
  }

  /**
   * @override
   */
  public canDismissNicoModalOnOkay() {
    return false;
  }

  /**
   * @override
   */
  public onNicoModalCancelClick() {

  }

  /**
   * @override
   */
  public onNicoModalClose() {
    this.setPageTitle('mod_manage.' + this.module + '.page_title');
  }

  /**
   * @override
   */
  public onNicoModalOkayClick() {

  }

  /**
   * @override
   */
  public setCancelProcessCompleteListener() {

  }

  /**
   * @override
   */
  public setNicoModalChildData(data: any) {
    this.service = data.service;
    this.resource = data.resource;
    this.module = data.module;
    if (!this.module) {
      this.module = 'mod_resellers';
    }
  }

  /**
   * @override
   */
  public setNicoModalDismisser(dismisser: Function) {
    this.modalDismisser = dismisser;
  }

  /**
   * @override
   */
  public setOkayProcessCompleteListener() {

  }

}
