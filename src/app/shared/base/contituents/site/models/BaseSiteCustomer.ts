import {AbstractBaseResource} from "../../../../../system/datamodels/AbstractBaseResource";

export class BaseSiteCustomer extends AbstractBaseResource{
    /**
     * Creatable attributes
     */
    public creatableAttributes = ['name','employee_number_range','user_number_range','office_address'];


}