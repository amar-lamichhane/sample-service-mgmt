import { TemplateRef } from '@angular/core';
import { Observable } from 'rxjs';

export interface NicoModalConfig {
    /**
     * The title of the modal box
     */
    modalTitle?: string | Observable<string>;
    /**
     * Custom modal title template
     */
    modalTitleTemplate?: TemplateRef<any>;
    /**
     * The modal footer template
     */
    modalFooterTemplate?: TemplateRef<any>;
    /***
     * The okay label
     */
    okayLabel?: string | Observable<string>;

    cancelLabel?: string | Observable<string>;

    showModalHeader?: boolean;

    coverImage?: boolean;

    showCancelButton?: boolean;

    showOkayButton?: boolean;

    showCrossButton?: boolean;

    htmlTemplateAsIs?: boolean;

    modalClass?: string;
    onOkayClick?: Function;
    onOkayProcessComplete?: Function;
    onCancelProcessComplete?: Function;
    onCancelClick?: Function;
    showModalFooter?: boolean,
    onClose?: Function;
    onDestroy?: Function;
    data?: any;

}
