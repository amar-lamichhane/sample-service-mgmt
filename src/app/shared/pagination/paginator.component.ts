import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';

@Component({
  selector: 'cmp-paginator',
  templateUrl: 'paginator.component.html'
})

export class PaginatorComponent implements OnInit, OnChanges {

  @Input() offset: number;
  /**
   * Per page limit
   */
  @Input() limit: number;
  /**
   * Total data
   */
  @Input() size: number;

  @Input() range: number = 1;
  /**
   * Current page
   */
  @Input() currentPage: number;

  /**
   * lazy loading option
   */
  @Input() lazyLoading: boolean = false;

  /**
   * Total pages
   */
  public totalPages: number;

  @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();

  pages: number[];

  constructor() {
  }

  ngOnInit() {
    this.getPages(this.offset, this.limit, this.size);
  }

  ngOnChanges() {
    this.getPages(this.offset, this.limit, this.size);
  }

  getPages(offset: number, limit: number, size: number) {
    this.totalPages = Math.ceil(this.size / this.limit);
    if(isNaN(this.totalPages)){
      this.totalPages = 0;
    }
    this.pages = new Array(this.totalPages);
  }

  selectPage(page: number) {
    if (this.isValidPageNumber(page, this.totalPages)) {
      this.pageChange.emit(page);
    }
  }

  isValidPageNumber(page: number, totalPages: number): boolean {
    return page > 0 && page <= totalPages;
  }

}
