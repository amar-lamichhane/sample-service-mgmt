import {BaseResourceInterface} from "../../../../../system/datamodels/BaseResourceInterface";

export interface BaseDocumentInterface extends BaseResourceInterface {
  id: number;
  title?: string;
  url?: string;
  description?: string;
  status?: number;
  order?: number;
  category_id?: number;
  created_at?: string;
  [key: string]: any;
}
