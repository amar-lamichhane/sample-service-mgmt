import {BaseResourceInterface} from "../../../../../system/datamodels/BaseResourceInterface";
import {StatusEnum} from "../../../../../system/enums/status.enum";
import {BaseUserInterface} from "../../user/interfaces/UserBaseInterface";

export interface BaseResellerInterface extends BaseResourceInterface {
  id: number;
  title?: string;
  description?: string;
  created_at?: string;
  updated_at?: string;
  logo_url?: string;
  sites_count?: string;
  domain?: string;
  message?: string;
  status?: StatusEnum;
  categories?: any;
  admin?: BaseUserInterface;
  [key: string]: any;
}
