export interface AdminSidebarMenuItemInterface {
    /**
     * the router link
     */
    routerLink: string;
    /**
     * title of the menu item
     */
    title: string;
    /**
     * Icon to be displayed with the menu item
     */
    icon: string;
    /**
     * The active inactive state of menu item
     */
    active?: boolean;
    /**
     * Visibility of menu item.
     */
    visible?: boolean;
}